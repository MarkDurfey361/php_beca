<?php
/* TEMA DE LAS VARIABLES */
$declaro_variable = "Así se declara y define una variable";
echo $declaro_variable; //Así se imprime en pantalla
/* Hay dos formas de imprimir variables */
echo "impresión con Echo";
print "impresión con Print";

/* AMBAS MANERAS PUEDEN TENER O NO SU TEXTO ENTRE PARÉNTESIS */

/* Las variables son sensibles a mayúsculas y minúsculas */
$DECLARO_VARIABLE = "Variable con mayúsculas (ver en código)<br>";

echo $DECLARO_VARIABLE;

//PHP Es un lenguaje no ti´pado, así que no necesitamos decir qué tipo es una variable.
$nombre = "Marco";
$edad = 23;

echo "Hola, soy " . $nombre . " y tengo ". $edad . " años.";

//-----------------------------------------------------------------------------
//CONSTANTES
define("nombre_constante", "valor de la constante", false);
/* El tercer parámetro (booleano) indica si es o no case sensitive, con false se hace sensible y con true no sensible*/
echo nombre_constante;
//-------------------------------------------------------------------------

//ALCANCE DE LAS VARIABLES 
/*
Hay locales y globales
*/

function prueba(){
	$local_variable="variable local";
	echo "A la variable dentro de una función se le llama : $local_variable";
}

prueba();

echo "Si intento acceder a una local desde fuera: $local_variable";
<p>Obtengo el siguiente error</p>
//Notice: Undefined variable: local in C:\xampp\htdocs\php\scripts\segundoscript.php on line 43

//------------------------------------------------------------------------
$var_externa = "variable externa o global";

function prueba2(){
	echo "A la variable externa dentro de una función se le llama: $var_externa";
	<p>Nótese el error que arroja<p>
	//Notice: Undefined variable: var_externa in C:\xampp\htdocs\php\scripts\segundoscript.php on line 49
}

prueba2();

echo "A la variable externa fuera de una función se le llama : $var_externa";

//------------------------------------------------------------------------
//PALABRA RESERVADA GLOBAL
echo '<br>';
echo '<br>GLOBAL<br>';

$global = 'global';
$global2 = 2;
$global3 = 3;

function prueba3(){

	global $global; //utilizamos la palabra global para poder ver la variable externa
	$notglobal = 'noglobal';
	$global = $global.$notglobal;
	echo '<br>';
	echo $global;
}

function prueba4(){
	$GLOBALS['global2'] = $GLOBALS['global2'] + $GLOBALS['global3'];
}

prueba3();

prueba4();
echo '<br>impresion prueba4<br>';
echo $global2;

//------------------------------------------------------------------------
//STATIC
echo '<br>';
echo '<br> STATIC <br>';

function pruebaStatic() {
  static $x = 0;
  echo 'dentro dela funcion'.$x;
  $x++;
}

pruebaStatic(); //output 0
echo "<br>";
pruebaStatic(); //output 1
echo "<br>";
pruebaStatic(); //output2