<?php
//incluir la conexión
include("conexion.php");

//Crear la consulta para cargar los productos de la BD
$consulta = "SELECT prod_id, nombre, marca, precio, observaciones FROM prods";

$ejecuta = $conexion -> query($consulta) or die("No se pueden recibir datos de productos <br>" . $conexion -> error);
 ?>

<table id="lista_productos">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
    <th>Marca</th>
    <th>Precio</th>
    <th>Observaciones</th>
    <th colspan="2">Operaciones</th>
  </tr>
  <?php
    while($arreglo_resultados = $ejecuta -> fetch_row()){
        echo "<tr>";
        echo "<td>" . $arreglo_resultados[0] . "</td>";
        echo "<td>" . $arreglo_resultados[1] . "</td>";
        echo "<td>" . $arreglo_resultados[2] . "</td>";
        echo "<td>" . $arreglo_resultados[3] . "</td>";
        echo "<td>" . $arreglo_resultados[4] . "</td>";
        echo "<td> <button type='button' onclick='formProducto(".$arreglo_resultados[0].");'>";
        echo "Editar</button></td>";
        echo "<td> <button type='button' value='Eliminar' onclick='eliminarProducto(".$arreglo_resultados[0].");'>";
        echo "Eliminar</button></td>";
        echo "</tr>";
    }
  ?>
</table>
