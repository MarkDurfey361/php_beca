-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-07-2020 a las 01:15:55
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `productos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prods`
--

CREATE TABLE `prods` (
  `prod_id` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `precio` float(8,2) NOT NULL,
  `marca` varchar(50) NOT NULL,
  `observaciones` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prods`
--

INSERT INTO `prods` (`prod_id`, `nombre`, `precio`, `marca`, `observaciones`) VALUES
(1, 'Mouse', 82.00, 'Logitech', 'Mouse inalambrico marca logitech 2'),
(2, 'Teclado', 120.00, 'Logitech', 'Teclado inalambrico marca logitech'),
(4, 'Audifonos', 790.00, 'Sony', 'AudÃ­fonos inalÃ¡mbricos');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `prods`
--
ALTER TABLE `prods`
  ADD PRIMARY KEY (`prod_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `prods`
--
ALTER TABLE `prods`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
